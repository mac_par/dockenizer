package com.page.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class PageView {
    @Id
    @GeneratedValue
    private Long id;
    private String pageUrl;
    private Date pageViewDate;
    private String correllationId;
}
