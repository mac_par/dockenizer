package com.page;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PageServiceApp {
    public static void main(String[] args) {
        SpringApplication.run(PageServiceApp.class, args);
    }
}
