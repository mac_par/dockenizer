package com.page.config;

import com.page.domain.PageView;
import com.page.model.PageViewEvent;
import com.page.repository.PageViewRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.integration.amqp.inbound.AmqpInboundChannelAdapter;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.integration.dsl.MessageChannels;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.MessageHandler;
import org.springframework.messaging.MessagingException;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import javax.xml.bind.JAXB;

@Slf4j
@EnableJpaRepositories(basePackages = {"com.page.repository"})
@EntityScan(basePackages = {"com.page.domain"})
@Configuration
public class RabbitConfig {
    public static final String INBOUND_QUEUE_NAME = "pageviewqueue";
    private static final String AMQP_CHANNEL = "amqpInputChannel";

    @Bean
    Queue queue() {
        return new Queue(INBOUND_QUEUE_NAME, false);
    }

    @Bean(name = AMQP_CHANNEL)
    MessageChannel amqpInputChannel() {
        return MessageChannels.direct().get();
    }

    @Bean
    SimpleMessageListenerContainer inboudContainer(ConnectionFactory connectionFactory) {
        SimpleMessageListenerContainer container = new SimpleMessageListenerContainer(connectionFactory);
        container.setQueueNames(INBOUND_QUEUE_NAME);
        return container;
    }

    @Bean
    AmqpInboundChannelAdapter amqpInboundChannelAdapter(SimpleMessageListenerContainer listenerContainer, @Qualifier(AMQP_CHANNEL) MessageChannel messageChannel) {
        AmqpInboundChannelAdapter adapter = new AmqpInboundChannelAdapter(listenerContainer);
        adapter.setOutputChannel(messageChannel);
        return adapter;
    }

    @Bean
    @ServiceActivator(inputChannel = AMQP_CHANNEL)
    MessageHandler pageViewMessageHandler(PageViewRepository repository) {
        return new MessageHandler() {
            @Override
            public void handleMessage(Message<?> message) throws MessagingException {
                log.info("Got message");
                String xmlString = (String) message.getPayload();
                log.info(xmlString);
                InputStream is = new ByteArrayInputStream(xmlString.getBytes(StandardCharsets.UTF_8));
                PageViewEvent pageViewEvent = JAXB.unmarshal(is, PageViewEvent.class);
                PageView pageView = new PageView();
                pageView.setPageUrl(pageViewEvent.getPageUrl());
                pageView.setPageViewDate(pageViewEvent.getPageViewDate());
                pageView.setCorrellationId(pageViewEvent.getCorrelationId());
                repository.saveAndFlush(pageView);
            }
        };
    }
}
