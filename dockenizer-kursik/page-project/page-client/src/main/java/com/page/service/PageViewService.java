package com.page.service;

import com.page.model.PageViewEvent;

public interface PageViewService {
    void sendPageViewEvent(PageViewEvent pageViewEvent);
}
