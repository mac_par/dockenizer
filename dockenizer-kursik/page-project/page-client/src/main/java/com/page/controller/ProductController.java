package com.page.controller;

import com.page.model.PageViewEvent;
import com.page.service.PageViewService;
import com.page.service.ProductService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Date;
import java.util.UUID;

@Slf4j
@Controller
public class ProductController {
    private final ProductService productService;
    private final PageViewService pageViewService;

    @Autowired
    public ProductController(ProductService productService, PageViewService pageViewService) {
        this.productService = productService;
        this.pageViewService = pageViewService;
    }

    @RequestMapping("product/{id}")
    public String getProductById(@PathVariable(name = "id") int id, Model model) {
        model.addAttribute("product", productService.getProduct(id));

        PageViewEvent pageViewEvent = PageViewEvent.builder().pageViewDate(new Date()).pageUrl("springframework.guru/product/" + id).correlationId(UUID.randomUUID().toString()).build();
        log.info("Sending message to page view service");
        pageViewService.sendPageViewEvent(pageViewEvent);

        return "product";
    }
}
