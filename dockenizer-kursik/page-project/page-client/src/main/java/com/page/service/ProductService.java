package com.page.service;

import com.page.domain.Product;

import java.util.List;

public interface ProductService {
    Product getProduct(Integer id);

    List<Product> listProducts();
}
