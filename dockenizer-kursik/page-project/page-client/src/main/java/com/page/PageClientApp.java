package com.page;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PageClientApp {
    public static void main(String[] args) {
        SpringApplication.run(PageClientApp.class, args);
    }
}
