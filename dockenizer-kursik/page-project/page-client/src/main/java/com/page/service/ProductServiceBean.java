package com.page.service;

import com.page.domain.Author;
import com.page.domain.Product;
import com.page.domain.ProductCategory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@Service
public class ProductServiceBean implements ProductService {
    private Map<Integer, Product> productMap;

    public ProductServiceBean() {
        loadProducts();
    }

    @Override
    public Product getProduct(Integer id) {
        return productMap.get(id);
    }

    @Override
    public List<Product> listProducts() {
        return List.copyOf(productMap.values());
    }

    private void loadProducts() {

        Author jt = new Author();
        jt.setFirstName("John");
        jt.setLastName("Thompson");
        jt.setId(1);
        jt.setImage("instructor_jt.jpg");

        ProductCategory springIntroCat = new ProductCategory();
        springIntroCat.setId(1);
        springIntroCat.setCategory("Spring Introduction");

        ProductCategory springCoreCat = new ProductCategory();
        springCoreCat.setId(2);
        springCoreCat.setCategory("Spring Core");

        ProductCategory springBootCat = new ProductCategory();
        springBootCat.setId(3);
        springBootCat.setCategory("Spring Boot");

        ProductCategory thymeleafCat = new ProductCategory();
        thymeleafCat.setId(4);
        thymeleafCat.setCategory("Thymeleaf");

        ProductCategory geapCat = new ProductCategory();
        geapCat.setId(5);
        geapCat.setCategory("G.E.A.P");

        productMap = new HashMap<>();

        Product springIntro = Product.builder()
                .id(1)
                .courseName("Introduction to Spring")
                .courseSubtitle("Start Learning Spring!")
                .author(jt)
                .courseDescription("Why would you want to learn about the Spring Framework? Simple, Spring is the most widely used framework in the enterprise today. Major companies all over the world are hiring programmers who know the Spring Framework.\n" +
                        "\n" +
                        "My Introduction Spring Framework Tutorial is designed to give you an introduction to the Spring Framework. This course is written for beginners. Ideally before taking the course, you should already have a foundation with the Java programming language. You don't need to be an expert in Java, but you should the basics of Object Oriented Programming with Java.\n" +
                        "\n" +
                        "You will learn what Dependency Injection is, and how Spring uses Inversion of Control to leverage Dependency Injection. Next in my course, I will walk you step by step through building your very first Spring Framework application. I'll show you hot to use the Spring Initializer and Spring Boot to jumpstart your Spring Framework project. Ideally, you can follow along and create your own Spring project. I know it can be frustrating to follow along in a course and run into errors. So don't worry, I have the complete source code examples in Git for you to checkout and use.")
                .price(new BigDecimal("0"))
                .imageUrl("SpringIntroThumb.png")
                .productCategoryList(List.of(springIntroCat, springBootCat)).build();
        productMap.put(1, springIntro);


        Product springCoreUltimate = Product.builder()
                .id(2)
                .courseName("Spring Core Ultimate")
                .courseSubtitle("Ultimate Bundle of Spring Core!")
                .author(jt)
                .courseDescription("Why would you want to learn about the Spring Framework? Simple, Spring is the most widely used framework in the enterprise today. Major companies all over the world are hiring programmers who know the Spring Framework.\n" +
                        "\n" +
                        "My Introduction Spring Framework Tutorial is designed to give you an introduction to the Spring Framework. This course is written for beginners. Ideally before taking the course, you should already have a foundation with the Java programming language. You don't need to be an expert in Java, but you should the basics of Object Oriented Programming with Java.\n" +
                        "\n" +
                        "You will learn what Dependency Injection is, and how Spring uses Inversion of Control to leverage Dependency Injection. Next in my course, I will walk you step by step through building your very first Spring Framework application. I'll show you hot to use the Spring Initializer and Spring Boot to jumpstart your Spring Framework project. Ideally, you can follow along and create your own Spring project. I know it can be frustrating to follow along in a course and run into errors. So don't worry, I have the complete source code examples in Git for you to checkout and use.")
                .price(new BigDecimal("199"))
                .imageUrl("SpringCoreUltimateThumb.png")
                .productCategoryList(List.of(springCoreCat, springBootCat)).build();
        productMap.put(2, springCoreUltimate);

        Product thymeleaf = Product.builder()
                .id(3)
                .courseName("Thymeleaf")
                .courseSubtitle("Thymeleaf and Spring!")
                .author(jt)
                .courseDescription("Why would you want to learn about the Spring Framework? Simple, Spring is the most widely used framework in the enterprise today. Major companies all over the world are hiring programmers who know the Spring Framework.\n" +
                        "\n" +
                        "My Introduction Spring Framework Tutorial is designed to give you an introduction to the Spring Framework. This course is written for beginners. Ideally before taking the course, you should already have a foundation with the Java programming language. You don't need to be an expert in Java, but you should the basics of Object Oriented Programming with Java.\n" +
                        "\n" +
                        "You will learn what Dependency Injection is, and how Spring uses Inversion of Control to leverage Dependency Injection. Next in my course, I will walk you step by step through building your very first Spring Framework application. I'll show you hot to use the Spring Initializer and Spring Boot to jumpstart your Spring Framework project. Ideally, you can follow along and create your own Spring project. I know it can be frustrating to follow along in a course and run into errors. So don't worry, I have the complete source code examples in Git for you to checkout and use.")
                .price(new BigDecimal("199"))
                .imageUrl("ThymeleafThumb.png")
                .productCategoryList(List.of(thymeleafCat)).build();
        productMap.put(3, thymeleaf);

        Product springCore = Product.builder()
                .id(4)
                .courseName("Spring Core")
                .courseSubtitle("Spring Core - mastering Spring!")
                .author(jt)
                .courseDescription("Why would you want to learn about the Spring Framework? Simple, Spring is the most widely used framework in the enterprise today. Major companies all over the world are hiring programmers who know the Spring Framework.\n" +
                        "\n" +
                        "My Introduction Spring Framework Tutorial is designed to give you an introduction to the Spring Framework. This course is written for beginners. Ideally before taking the course, you should already have a foundation with the Java programming language. You don't need to be an expert in Java, but you should the basics of Object Oriented Programming with Java.\n" +
                        "\n" +
                        "You will learn what Dependency Injection is, and how Spring uses Inversion of Control to leverage Dependency Injection. Next in my course, I will walk you step by step through building your very first Spring Framework application. I'll show you hot to use the Spring Initializer and Spring Boot to jumpstart your Spring Framework project. Ideally, you can follow along and create your own Spring project. I know it can be frustrating to follow along in a course and run into errors. So don't worry, I have the complete source code examples in Git for you to checkout and use.")
                .price(new BigDecimal("199"))
                .imageUrl("SpringCoreThumb.png")
                .productCategoryList(List.of(springCoreCat, springBootCat)).build();
        productMap.put(4, springCore);

        Product springCoreAdv = Product.builder()
                .id(5)
                .courseName("Spring Core Advanced")
                .courseSubtitle("Advanced Spring Core!")
                .author(jt)
                .courseDescription("Why would you want to learn about the Spring Framework? Simple, Spring is the most widely used framework in the enterprise today. Major companies all over the world are hiring programmers who know the Spring Framework.\n" +
                        "\n" +
                        "My Introduction Spring Framework Tutorial is designed to give you an introduction to the Spring Framework. This course is written for beginners. Ideally before taking the course, you should already have a foundation with the Java programming language. You don't need to be an expert in Java, but you should the basics of Object Oriented Programming with Java.\n" +
                        "\n" +
                        "You will learn what Dependency Injection is, and how Spring uses Inversion of Control to leverage Dependency Injection. Next in my course, I will walk you step by step through building your very first Spring Framework application. I'll show you hot to use the Spring Initializer and Spring Boot to jumpstart your Spring Framework project. Ideally, you can follow along and create your own Spring project. I know it can be frustrating to follow along in a course and run into errors. So don't worry, I have the complete source code examples in Git for you to checkout and use.")
                .price(new BigDecimal("199"))
                .imageUrl("SpringCoreAdvancedThumb.png")
                .productCategoryList(List.of(springCoreCat, springBootCat)).build();
        productMap.put(5, springCoreAdv);

        Product springCoreDevOps = Product.builder()
                .id(6)
                .courseName("Spring Core Dev-Ops")
                .courseSubtitle("Deploying Spring in the Enterprise and the cloud!")
                .author(jt)
                .courseDescription("Why would you want to learn about the Spring Framework? Simple, Spring is the most widely used framework in the enterprise today. Major companies all over the world are hiring programmers who know the Spring Framework.\n" +
                        "\n" +
                        "My Introduction Spring Framework Tutorial is designed to give you an introduction to the Spring Framework. This course is written for beginners. Ideally before taking the course, you should already have a foundation with the Java programming language. You don't need to be an expert in Java, but you should the basics of Object Oriented Programming with Java.\n" +
                        "\n" +
                        "You will learn what Dependency Injection is, and how Spring uses Inversion of Control to leverage Dependency Injection. Next in my course, I will walk you step by step through building your very first Spring Framework application. I'll show you hot to use the Spring Initializer and Spring Boot to jumpstart your Spring Framework project. Ideally, you can follow along and create your own Spring project. I know it can be frustrating to follow along in a course and run into errors. So don't worry, I have the complete source code examples in Git for you to checkout and use.")
                .price(new BigDecimal("199"))
                .imageUrl("SpringCoreDevOpsThumb.png")
                .productCategoryList(List.of(springCoreCat, springBootCat)).build();
        productMap.put(6, springCoreDevOps);

    }
}
