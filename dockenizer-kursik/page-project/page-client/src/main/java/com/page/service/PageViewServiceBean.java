package com.page.service;

import com.page.config.AppConfig;
import com.page.model.PageViewEvent;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.StringWriter;
import javax.xml.bind.JAXB;

@Slf4j
@Service
public class PageViewServiceBean implements PageViewService {
    private final RabbitTemplate rabbitTemplate;

    @Autowired
    public PageViewServiceBean(RabbitTemplate rabbitTemplate) {
        this.rabbitTemplate = rabbitTemplate;
    }

    @Override
    public void sendPageViewEvent(PageViewEvent pageViewEvent) {
        String xmlString;
        try (StringWriter xmlPayload = new StringWriter()) {
            JAXB.marshal(pageViewEvent, xmlPayload);
            xmlString = xmlPayload.toString();
        } catch (IOException e) {
            log.error("dupa", e);
            throw new RuntimeException(e);
        }

        log.info("Sending event payload {}", xmlString);

        rabbitTemplate.convertAndSend(AppConfig.OUTBOUND_QUEUE_NAME, xmlString);
        rabbitTemplate.convertAndSend(AppConfig.OUTBOUND_AUDIT_QUEUE_NAME, xmlString);
    }
}
