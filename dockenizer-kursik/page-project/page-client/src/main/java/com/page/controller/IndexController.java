package com.page.controller;

import com.page.model.PageViewEvent;
import com.page.service.PageViewService;
import com.page.service.ProductService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Date;
import java.util.UUID;

@Slf4j
@Controller
public class IndexController {
    private final ProductService productService;
    private final PageViewService pageViewService;

    public IndexController(ProductService productService, PageViewService pageViewService) {
        this.productService = productService;
        this.pageViewService = pageViewService;
    }

    @RequestMapping({"/", "index"})
    public String getIndex(Model model) {
        model.addAttribute("products", productService.listProducts());
        PageViewEvent pageViewEvent = PageViewEvent.builder().correlationId(UUID.randomUUID().toString())
                .pageUrl("springframework.guru/")
                .pageViewDate(new Date()).build();
        log.info("Sending message to page view service");
        pageViewService.sendPageViewEvent(pageViewEvent);
        return "index";
    }
}
