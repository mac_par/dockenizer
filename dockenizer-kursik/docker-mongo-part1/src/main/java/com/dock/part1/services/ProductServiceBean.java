package com.dock.part1.services;

import com.dock.part1.commands.ProductForm;
import com.dock.part1.converters.ProductFromToProduct;
import com.dock.part1.domain.Product;
import com.dock.part1.repositories.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ProductServiceBean implements ProductService {
    private ProductRepository productRepository;
    private ProductFromToProduct mapper;

    @Autowired
    public ProductServiceBean(ProductRepository productRepository, ProductFromToProduct mapper) {
        this.productRepository = productRepository;
        this.mapper = mapper;
    }

    @Override
    public List<Product> listAll() {
        return new ArrayList<>(productRepository.findAll());
    }

    @Override
    public Product getById(String id) {
        return productRepository.findById(id).orElse(null);
    }

    @Override
    public Product saveOrUpdate(Product product) {
        return productRepository.save(product);
    }

    @Override
    public void delete(String id) {
        productRepository.deleteById(id);
    }

    @Override
    public Product saveOrUpdateProductForm(ProductForm productForm) {
        return productRepository.save(mapper.convert(productForm));
    }
}
