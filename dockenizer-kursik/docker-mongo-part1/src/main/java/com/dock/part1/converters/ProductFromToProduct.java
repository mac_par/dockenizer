package com.dock.part1.converters;

import com.dock.part1.commands.ProductForm;
import com.dock.part1.domain.Product;
import org.bson.types.ObjectId;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

@Component
public class ProductFromToProduct implements Converter<ProductForm, Product> {
    @Override
    public Product convert(ProductForm productForm) {
        Product product = Product.builder()
                .description(productForm.getDescription())
                .price(productForm.getPrice())
                .imageUrl(productForm.getImageUrl())
                .build();
        if (!StringUtils.isEmpty(productForm.getId())) {
            product.set_id(new ObjectId(productForm.getId()));
        }

        return product;
    }
}
