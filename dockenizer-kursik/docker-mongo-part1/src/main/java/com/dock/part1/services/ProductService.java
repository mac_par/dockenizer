package com.dock.part1.services;

import com.dock.part1.commands.ProductForm;
import com.dock.part1.domain.Product;

import java.util.List;

public interface ProductService {
    List<Product> listAll();

    Product getById(String id);

    Product saveOrUpdate(Product product);

    void delete(String id);

    Product saveOrUpdateProductForm(ProductForm productForm);
}
