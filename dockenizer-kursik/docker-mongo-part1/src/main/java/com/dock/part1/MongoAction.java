package com.dock.part1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@SpringBootApplication
@EnableMongoRepositories(basePackages = {"com.dock.part1.repositories"})
@EnableWebMvc
public class MongoAction {
    public static void main(String[] args) {
        SpringApplication.run(MongoAction.class, args);
    }
}
