package com.dock.part1.domain;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.math.BigDecimal;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Document
public class Product {
    @Id
    @Getter(value = AccessLevel.NONE)
    private ObjectId _id;
    private String description;
    private BigDecimal price;
    private String imageUrl;

    public ObjectId getId() {
        return _id;
    }
}
