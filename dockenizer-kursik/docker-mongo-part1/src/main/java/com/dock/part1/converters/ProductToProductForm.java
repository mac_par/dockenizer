package com.dock.part1.converters;

import com.dock.part1.commands.ProductForm;
import com.dock.part1.domain.Product;
import org.springframework.stereotype.Component;
import org.springframework.core.convert.converter.Converter;

@Component
public class ProductToProductForm implements Converter<Product, ProductForm> {
    @Override
    public ProductForm convert(Product product) {
        return ProductForm.builder()
                .id(product.getId().toHexString())
                .description(product.getDescription())
                .imageUrl(product.getImageUrl())
                .price(product.getPrice())
                .build();
    }
}
