FROM php:8.0-fpm-alpine

WORKDIR /var/www/html
ADD https://raw.githubusercontent.com/mlocati/docker-php-extension-installer/master/install-php-extensions /usr/local/bin/
RUN chmod uga+x /usr/local/bin/install-php-extensions && sync && \
    install-php-extensions pdo pdo_mysql
#RUN addgroup -g 100 laravel && adduser -G laravel -g laravel -s /bin/sh -D laravel
#USER laravel
COPY src .
RUN chown -R www-data:www-data /var/www/html
#RUN docker-php-ext-install pdo pdo-mysql