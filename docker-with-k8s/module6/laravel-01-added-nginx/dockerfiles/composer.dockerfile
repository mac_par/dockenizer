FROM composer:2.0
RUN addgroup -g 100 laravel && adduser -G laravel -g laravel -s /bin/sh -D laravel
USER laravel
WORKDIR /var/www/html
ENTRYPOINT ["composer", "--ignore-platform-reqs"]