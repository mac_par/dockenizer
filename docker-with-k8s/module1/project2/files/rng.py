from random import randint

min_number = int(input('Please provide min value: '))
max_number = int(input('Please provide max value: '))

if (max_number < min_number):
    print('Invalid input - shutting down...')
else:
    print(randint(min_number, max_number))
