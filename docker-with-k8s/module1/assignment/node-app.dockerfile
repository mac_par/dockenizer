FROM node:20

WORKDIR /app
EXPOSE 3000
COPY node-app/package.json /app
RUN npm install package.json
COPY node-app/server.js /app
CMD ["node", "server.js"]