FROM python:3.11
WORKDIR /app
COPY python-app/bmi.py /app
CMD ["python", "bmi.py"]