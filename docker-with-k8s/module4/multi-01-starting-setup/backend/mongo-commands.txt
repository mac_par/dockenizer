mongo:4.2
volume mongo

docker container run -d --rm --name mongo -v mongo:/data/db --network module4 -p 27017:27017 --env-file ./mongo.env mongo:4.2